

// import * as hello from "../markdown/test.md";
import * as flowField from "../projects/flow_field/index.js";
import * as webGl from "../projects/webgl/index.js";
import * as sensation from "../projects/sensation/index.js";
import * as triangles from "../projects/triangles/index.js";

import * as hello from "../projects/hello/index.js";
import * as substrate from "../projects/substrate/index.js";
import * as diploma from "../projects/diploma/index.js";

import * as websites from "../projects/websites/index.js";
import * as speakers from "../projects/speakers/index.js";
import * as leberkas from "../projects/leberkas/index.js";
// import * as stippling from "../projects/stippling/index.js";
import * as dress from "../projects/dress/index.js";
import * as ladonaira from "../projects/ladonaira/index.js";
import * as kitchen from "../projects/kitchen/index.js";
import * as misc from "../projects/misc/index.js";


import {htmlToElement} from "./util"

function init(container) {


    hello.init(container);
    flowField.init(container);
    websites.init(container);
    webGl.init(container);
    ladonaira.init(container);
    diploma.init(container);
    substrate.init(container);
    kitchen.init(container);
    triangles.init(container);
    dress.init(container);
    sensation.init(container);
    leberkas.init(container);
    speakers.init(container);
    misc.init(container);
    
}

export { init };