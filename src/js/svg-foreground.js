import { default as LeonSans }  from '../../assets/projects/hello/leonsans/leonsans'


const text = 'obacht'

const outlines = new LeonSans({
    text: text
    , color: ['#EEDD82']
    , size: 328
    , weight: 800
    , tracking: -0.12
})

const fillins = new LeonSans({
    text: text
    , color: ['#DC4F39']
    , size: 320
    , weight: 200
    , tracking: 0
})

let canvas, ctx, width, height

function init(container, w, h) {
    const pixelRatio = window.devicePixelRatio

    width = w
    height = h
    canvas = document.createElement('canvas');
    container.appendChild(canvas);
    ctx = canvas.getContext("2d");

    canvas.width = width * pixelRatio
    canvas.height = height * pixelRatio
    canvas.style.width = width + 'px';
    canvas.style.height = height + 'px';
    ctx.scale(pixelRatio, pixelRatio);

    requestAnimationFrame(animate);
}

function animate(t) {
    requestAnimationFrame(animate);
    render();
}

function renderFillins() {
    const x = (width - fillins.rect.w) / 2;
    const y = (height - fillins.rect.h) / 2;
    fillins.position(x, y);
    fillins.draw(ctx);
}

function renderOutlines() {
    const x = (width - outlines.rect.w) / 2;
    const y = (height - outlines.rect.h) / 2;
    outlines.position(x, y);
    outlines.draw(ctx);
}

function render() {
    ctx.clearRect(0, 0, width, height);
    renderOutlines()
    renderFillins()
}



export { init }