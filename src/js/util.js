function htmlToElement(html) {
  var template = document.createElement("template");
  html = html.trim(); // Never return a text node of whitespace as the result
  template.innerHTML = html;
  return template.content.firstChild;
}

function htmlToElements(html) {
  var template = document.createElement("template");
  template.innerHTML = html;
  return template.content.childNodes;
}

// proce55ing map function
function map(value, istart, istop, ostart, ostop) {
  return ostart + (ostop - ostart) * ((value - istart) / (istop - istart));
}


// lodash radnom function
// @see: https://github.com/lodash/lodash/blob/master/random.js
function random(lower, upper, floating) {
  if (floating === undefined) {
    if (typeof upper == 'boolean') {
      floating = upper
      upper = undefined
    }
    else if (typeof lower == 'boolean') {
      floating = lower
      lower = undefined
    }
  }
  if (lower === undefined && upper === undefined) {
    lower = 0
    upper = 1
  }
  else {
    if (upper === undefined) {
      upper = lower
      lower = 0
    }
  }
  if (lower > upper) {
    const temp = lower
    lower = upper
    upper = temp
  }
  if (floating || lower % 1 || upper % 1) {
    const rand = Math.random()
    const randLength = `${rand}`.length - 1
    return Math.min(lower + (rand * (upper - lower + freeParseFloat(`1e-${randLength}`)), upper))
  }
  return lower + Math.floor(Math.random() * (upper - lower + 1))
}

export { htmlToElement, htmlToElements, map, random };