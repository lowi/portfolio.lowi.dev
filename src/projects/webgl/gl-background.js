
import * as THREE from 'three'
import {Noise} from 'noisejs'
import frag from './shaders/frag.glsl'
import vert from './shaders/vert.glsl'

const renderer = new THREE.WebGLRenderer()
const camera = new THREE.Camera()
const scene = new THREE.Scene()
const uniforms = {
    time: { type: "f", value: 1.0 },
    displace: { type: "f", value: 0 },
    resolution: { type: "v2", value: new THREE.Vector2() }
}
const material = new THREE.ShaderMaterial({
    uniforms: uniforms,
    vertexShader: vert,
    fragmentShader: frag
})
const mesh = new THREE.Mesh(new THREE.PlaneGeometry(2, 2), material)
const noise = new Noise(Math.random())
var time = 0


function init(container, width, height) {
    camera.position.z = 1
    scene.add(mesh)
    
    renderer.setPixelRatio(window.devicePixelRatio ? window.devicePixelRatio : 1)
    container.appendChild(renderer.domElement)
    uniforms.resolution.value.x = width
    uniforms.resolution.value.y = height
    renderer.setSize(width, height)
    
    animate()
}

function animate() {
    requestAnimationFrame(animate)
    render()
}

function render() {
    const δT = (noise.perlin2(0, time) + 1.) * 0.001
    const δD = Math.abs(noise.perlin2(4, δT)) * 20
    uniforms.time.value = time
    uniforms.displace.value = δD
    time += δT
    renderer.render(scene, camera)
}

export { init }