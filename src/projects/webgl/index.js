import * as html from "./index.html";
import {htmlToElement} from "../../../src/js/util.js"
import { init as initGl } from "./gl-background";


function init(container) {
    var node = htmlToElement(html);
    container.appendChild(node);
     initGl(
       document.getElementById("webgl-background"),
       window.innerWidth,
       window.innerHeight
     );
}

export { init };
