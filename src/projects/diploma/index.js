import * as html from "./index.html";
import {htmlToElement} from "../../../src/js/util.js"
import * as thesis from "./thesis.pdf";


function init(container) {
    var node = htmlToElement(html);
    container.appendChild(node);
}

export { init };
