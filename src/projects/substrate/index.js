import * as html from "./index.html";
import {htmlToElement, map} from "../../../src/js/util.js"

let canvas, ctx, width, height;

/*
stolen from: 
j.tarbell   
June, 2004
complexification.net
*/

var dimx, dimy;
var num = 0;
const MAXNUM = 12;

const NUM_CRACKS = 5;
var cracks = [];
var cgrid = [];


// ███████╗ █████╗ ███╗   ██╗██████╗ ██████╗  █████╗ ██╗███╗   ██╗████████╗███████╗██████╗ 
// ██╔════╝██╔══██╗████╗  ██║██╔══██╗██╔══██╗██╔══██╗██║████╗  ██║╚══██╔══╝██╔════╝██╔══██╗
// ███████╗███████║██╔██╗ ██║██║  ██║██████╔╝███████║██║██╔██╗ ██║   ██║   █████╗  ██████╔╝
// ╚════██║██╔══██║██║╚██╗██║██║  ██║██╔═══╝ ██╔══██║██║██║╚██╗██║   ██║   ██╔══╝  ██╔══██╗
// ███████║██║  ██║██║ ╚████║██████╔╝██║     ██║  ██║██║██║ ╚████║   ██║   ███████╗██║  ██║
// ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚═════╝ ╚═╝     ╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝   ╚═╝   ╚══════╝╚═╝  ╚═╝
                                                                                        

class SandPainter {

  constructor() {
    this.c = somecolor();
    this.g = map(Math.random(), 0, 1, 0.01, 0.1);
  }

  render(x, y, ox, oy) {
    // modulate gain
    this.g += (Math.random() * 2 - 1) * 0.050;
    var maxg = 1.0;
    if (this.g<0) this.g=0;
    if (this.g>maxg) this.g=maxg;
    
    // calculate grains by distance
    //int grains = int(sqrt((ox-x)*(ox-x)+(oy-y)*(oy-y)));
    var grains = 64;

    // lay down grains of sand (transparent pixels)
    var w = this.g/(grains-1);
    for (i=0;i<grains;i++) {
      var a = 0.1-i/(grains*10.0);
    //   stroke(red(c),green(c),blue(c),a*256);
    //   point(ox+(x-ox)*sin(sin(i*w)),oy+(y-oy)*sin(sin(i*w)));
    }
  }
}



//  ██████╗██████╗  █████╗  ██████╗██╗  ██╗
// ██╔════╝██╔══██╗██╔══██╗██╔════╝██║ ██╔╝
// ██║     ██████╔╝███████║██║     █████╔╝ 
// ██║     ██╔══██╗██╔══██║██║     ██╔═██╗ 
// ╚██████╗██║  ██║██║  ██║╚██████╗██║  ██╗
//  ╚═════╝╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
                                        

class Crack {

    constructor() {
    // find placement along existing crack
    this.findStart();
    // this.sp = new SandPainter();
  }
  
  findStart() {
        // pick random point
        var px=0;
        var py=0;

        // shift until crack is found
        var found = false;
        var timeout = 0;
        while ((!found) || (timeout++>1000)) {
            px = Math.round(Math.random() * dimx);
            py = Math.round(Math.random() * dimy);
            if (cgrid[py*dimx+px]<10000) {
            found=true;
            }
        }

        if (found) {
            // start crack
            var a = cgrid[py*dimx+px];
            if (Math.random() < 0.5) {
              a -= 90 + map(Math.random(), 0, 1, -2, 2.1);
            } else {
              a += 90 + map(Math.random(), 0, 1, -2, 2.1);
            }
            this.startCrack(px,py,a);
        }
    }
   
    startCrack(x, y, t) {
        this.x=x;
        this.y=y;
        this.t=t;//%360;
        this.x += 0.61 * Math.cos(this.t * Math.PI / 180);
        this.y += 0.61 * Math.sin(this.t * Math.PI / 180);  
    }
             
    move() {
        // continue cracking
        this.x += 0.42 * Math.cos((this.t * Math.PI) / 180);
        this.y += 0.42 * Math.sin((this.t * Math.PI) / 180); 
        
        var cx = Math.round(this.x);
        var cy = Math.round(this.y);
    
        // draw black crack
        ctx.fillStyle = "rgba(0, 0, 0, 0.38)";
        ctx.fillRect(this.x, this.y, 1, 1);

    
    
        if ((cx>=0) && (cx<dimx) && (cy>=0) && (cy<dimy)) {
            // safe to check
            if (
                cgrid[cy * dimx + cx] > 10000 ||
                Math.abs(cgrid[cy * dimx + cx] - this.t) < 5
            ) {
                // continue cracking
                cgrid[cy * dimx + cx] = Math.round(this.t);
            } else if (Math.abs(cgrid[cy * dimx + cx] - this.t) > 2) {
                // crack encountered (not self), stop cracking
                this.findStart();
                makeCrack();
            }
        } else {
            // out of bounds, stop cracking
            this.findStart();
            makeCrack();
        }
    }
}




function somecolor() {
  // pick some random good color
  return "#000000";
}


function animate(t) {
    requestAnimationFrame(animate);
    render();
    //   count++;
    // console.log("cracks", cracks[0]);
}


function render() {
    // crack all cracks
    for (var n=0;n<num;n++) {
        cracks[n].move();
    }
}

function begin() {
    // erase crack grid
    for (var y=0;y<dimy;y++) {
        for (var x=0;x<dimx;x++) {
            cgrid[y*dimx+x] = 10001;
        }
    }
    // make random crack seeds
    for (var k = 0;k<16;k++) {
        var i = Math.round(Math.random() * dimx*dimy-1);
        cgrid[i]=Math.round(Math.random() * 360);
    }

    // make NUM_CRACKScracks
    num=0;
    for (var k=0;k<NUM_CRACKS;k++) {
        makeCrack();
    }
    ctx.clearRect(0, 0, dimx, dimy);
}

function makeCrack() {
    if (num < MAXNUM) {
        // make a new crack instance
        cracks[num] = new Crack();
        num++;
    }
}


function initCanvas(container) {
    const pixelRatio = window.devicePixelRatio;

    canvas = document.createElement("canvas");
    container.appendChild(canvas);
    ctx = canvas.getContext("2d");

    canvas.width = dimx * pixelRatio;
    canvas.height = dimy * pixelRatio;
    canvas.style.width = dimx + "px";
    canvas.style.height = dimy + "px";
    ctx.scale(pixelRatio, pixelRatio);
}

function init(container) {
    var node = htmlToElement(html);
    dimx = window.innerWidth;
    dimy = window.innerHeight/2;
    container.appendChild(node);
    initCanvas(node);
    begin();
    requestAnimationFrame(animate);
   
}

export { init };
