import * as html from "./index.html";
import {htmlToElement, map} from "../../js/util.js"
import {Noise} from "noisejs";

function init(container) {
    var node = htmlToElement(html);
    container.appendChild(node);
}

export { init };
