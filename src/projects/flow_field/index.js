import * as html from "./index.html";
import { htmlToElement } from "../../../src/js/util.js";

function init(container) {
  var node = htmlToElement(html);
  container.appendChild(node);
}

export { init };
