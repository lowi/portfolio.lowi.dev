import * as html from "./index.html";
import {htmlToElement, map} from "../../../src/js/util.js"
import { default as LeonSans } from "./leonsans/leonsans";
import {Noise} from "noisejs";

const _text = "Lowi";
const _arrow = "I\nI\nI\nV";

const arrow = new LeonSans({
    text: _arrow,
    color: ["#DC4F39"],
    size: 64,
    weight: 200,
    align: 'center',
    leading: -9.5,
});

const text = new LeonSans({
    text: _text,
    color: ["#DC4F39"],
    size: 192,
    weight: 200,
    tracking: 0,
});

let canvas, ctx, width, height;
var noise = new Noise(Math.random());
var count = 0;
var NOISE_STEP = (value) => value * 0.0002;
var WIDTH_SCALE = (value) => map(value, -1, 1, 100, 1000);
var TRACK_SCALE = (value) => map(value, -1, 1, -2, 5);


function animate(t) {
    requestAnimationFrame(animate);
    render();
    count++;
}

function rendertext() {
    const x = (width - text.rect.w) * 0.38;
    const y = (height - text.rect.h) / 2;
    const weight = WIDTH_SCALE(noise.simplex2(NOISE_STEP(count), 0.4));    
    const tracking = TRACK_SCALE(noise.simplex2(NOISE_STEP(count), 0.5));    

    text.weight = weight;
    text.tracking = tracking;
    text.position(x, y);
    text.draw(ctx);
}

function renderArrow() {
    // const x = (width - arrow.rect.w) *0.28;
    const x = 120;
    const y = (height - arrow.rect.h) *0.81;
    const weight = WIDTH_SCALE(noise.simplex2(NOISE_STEP(count), 0.7));      
    
    arrow.weight = weight;
    arrow.position(x, y);
    arrow.draw(ctx);
}

function render() {
    ctx.clearRect(0, 0, width, height);
    renderArrow();
    rendertext();
}


function initCanvas(container, w, h) {
    const pixelRatio = window.devicePixelRatio;

    width = w;
    height = h;
    canvas = document.createElement("canvas");
    container.appendChild(canvas);
    ctx = canvas.getContext("2d");

    canvas.width = width * pixelRatio;
    canvas.height = height * pixelRatio;
    canvas.style.width = width + "px";
    canvas.style.height = height + "px";
    ctx.scale(pixelRatio, pixelRatio);

}

function init(container) {
    var node = htmlToElement(html);
    container.appendChild(node);
    initCanvas(node, window.innerWidth, window.innerHeight);
    requestAnimationFrame(animate);

}

export { init };
