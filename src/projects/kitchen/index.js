import * as html from "./index.html";
import {htmlToElement} from "../../js/util.js"

// require("./wohnung_01.png");
// require("./Annotation_02.png");

function init(container) {
    var node = htmlToElement(html);
    container.appendChild(node);
}

export { init };
