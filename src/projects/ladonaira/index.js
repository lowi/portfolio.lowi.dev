import * as html from "./index.html";
import {htmlToElement} from "../../js/util.js"
import simpleParallax from "simple-parallax-js";

require("./la-donaira-starry-ski.jpg");
require("./la-donaira-living-room.jpg");
require("./la-donaira-panorama.jpg");
require("./la-donaira-nature.jpg");
require("./la-donaira-1.jpg");

function init(container) {
    var node = htmlToElement(html);
    container.appendChild(node);

    var images = container.querySelectorAll(".parallax");
    new simpleParallax(images);
}

export { init };
