import * as html from "./index.html";
import * as gradientsImage from "./gradients.png";
import {htmlToElement, map} from "../../js/util.js"

import stippling from './stippling'

let canvas, ctx, dimx, dimy;

console.log("gradientsImage", gradientsImage);

function animate(t) {
    requestAnimationFrame(animate);
    render();
}


function render() {
  
}

function begin() {
    // ctx.clearRect(0, 0, dimx, dimy);
}


function initCanvas(container) {
    const pixelRatio = window.devicePixelRatio;

    // canvas = document.createElement("canvas");
    // container.appendChild(canvas);
    // ctx = canvas.getContext("2d");

    // canvas.width = dimx * pixelRatio;
    // canvas.height = dimy * pixelRatio;
    // canvas.style.width = dimx + "px";
    // canvas.style.height = dimy + "px";
    // ctx.scale(pixelRatio, pixelRatio);

    stippling.init(container, gradientsImage, {})
}

function init(container) {
    var node = htmlToElement(html);
    dimx = window.innerWidth;
    dimy = window.innerHeight;
    container.appendChild(node);
    initCanvas(node);
    begin();
    requestAnimationFrame(animate);
   
}

export { init };
