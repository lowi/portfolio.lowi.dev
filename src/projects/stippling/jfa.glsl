const float c_gamma = 2.2;
const float KEY_1 = 49.5/256.0;

//============================================================
void DecodeData (in vec4 data, out vec2 coord, out vec3 color) {
    coord = data.xy;
    color.x = floor(data.z / 256.0) / 255.0;
    color.y = mod(data.z, 256.0) / 255.0;
    color.z = mod(data.w, 256.0) / 255.0; }

//============================================================
void main(void) {

  // get the data for this pixel
  vec2 uv = -1.0 + 2.0 *vUv;
  vec4 data = texture( iChannel0, uv);

  // decode this pixel
  vec2 seedCoord;
  vec3 seedColor;
  DecodeData(data, seedCoord, seedColor);

  // highlight the seeds a bit
  if(length(fragCoord-seedCoord) > 5.0) seedColor *= 0.75;
  
  // if the 1 key is pressed, show distance info instead
  if(texture(iChannel1, vec2(KEY_1,0.25)).x > 0.1) {
    float dist = length(seedCoord - fragCoord) / 25.0;
    seedColor = vec3(dist); }      
    
  // gamma correct
  seedColor = pow(seedColor, vec3(1.0/c_gamma));
  fragColor = vec4(seedColor, 1.0); }


// chance of a pixel being a seed, assuming rand() is a good rng
const float c_seedChance = 0.0005;

// how many frames between steps.  1.0 = full speed.
const float c_frameStepDelay = 12.0;

// how many JFA steps to do.  2^c_maxSteps is max image size on x and y
const float c_maxSteps = 10.0;

//============================================================
// Hash without sine from https://www.shadertoy.com/view/4djSRW
#define HASHSCALE1 .1031
float hash12(vec2 p) {
  vec3 p3  = fract(vec3(p.xyx) * HASHSCALE1);
  p3 += dot(p3, p3.yzx + 19.19);
  return fract((p3.x + p3.y) * p3.z); }

//============================================================
// returns 0..1
float Rand(vec2 co) {
  co.x += fract(iDate.w * 5.342);
  co.y += fract(iDate.w * 11.214);
  return hash12(co); }

//============================================================
vec3 RandColor (vec2 co) {
  return vec3 (
    Rand(co),
    Rand(co*2.143),
    Rand(co*3.163) ); }

//============================================================
vec4 EncodeData (in vec2 coord, in vec3 color) {
  vec4 ret = vec4(0.0);
  ret.xy = coord;
  ret.z = floor(color.x * 255.0) * 256.0 + floor(color.y * 255.0);
  ret.w = floor(color.z * 255.0);
  return ret; }

//============================================================
void DecodeData (in vec4 data, out vec2 coord, out vec3 color) {
  coord = data.xy;
  color.x = floor(data.z / 256.0) / 255.0;
  color.y = mod(data.z, 256.0) / 255.0;
  color.z = mod(data.w, 256.0) / 255.0; }

//============================================================
vec4 StepJFA (in vec2 fragCoord, in float level) {
  level = clamp(level-1.0, 0.0, c_maxSteps);
  float stepwidth = floor(exp2(c_maxSteps - level)+0.5);
  
  float bestDistance = 9999.0;
  vec2 bestCoord = vec2(0.0);
  vec3 bestColor = vec3(0.0);
  
  for (int y = -1; y <= 1; ++y) {
    for (int x = -1; x <= 1; ++x) {
      vec2 sampleCoord = fragCoord + vec2(x,y) * stepwidth;
      
      vec4 data = texture( iChannel0, sampleCoord / iChannelResolution[0].xy);
      vec2 seedCoord;
      vec3 seedColor;
      DecodeData(data, seedCoord, seedColor);
      float dist = length(seedCoord - fragCoord);
      if ((seedCoord.x != 0.0 || seedCoord.y != 0.0) && dist < bestDistance) {
        bestDistance = dist;
        bestCoord = seedCoord;
        bestColor = seedColor; }}}
  return EncodeData(bestCoord, bestColor); }

//============================================================
void main(void) {
  // initialize random seed locations and colors
  if (iFrame == 0) {
    if (Rand(fragCoord) > (1.0 - c_seedChance))
      fragColor = EncodeData(fragCoord, RandColor(fragCoord));
    else
      fragColor = vec4(0.0);
    return;
  }
  
  // periodic steps
  if (mod(float(iFrame), c_frameStepDelay) == 0.0)
    fragColor = StepJFA(fragCoord, floor(float(iFrame) / c_frameStepDelay));
  else
    fragColor = texture( iChannel0, fragCoord.xy / iChannelResolution[0].xy); }