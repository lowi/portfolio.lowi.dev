import {Clock,
        Color,
        DataTexture,
        LinearFilter,
        NearestFilter,
        RepeatWrapping,
        RGBFormat,
        RGBAFormat,
        ShaderMaterial,
        Texture,
        UniformsUtils,
        Vector2,
        Vector3,
        Vector4,
        WebGLRenderer,
        WebGLRenderTarget } from 'three'

import {
  EffectComposer,
  ShaderPass,
  SavePass,
  TextureEffect,
} from "postprocessing";

import _                from 'lodash'
import μgl              from './gl-util'
import StipplingShaders from './stippling-shaders'


function _makePointsTexture(dimensions, points) {
  // create a square blank texture with the given dimensions
  let texture = μgl.makeDataTexture(dimensions)
  
  // set the points pixels to their respective colors in the points texture
  _.each(points, p => {
    let i = p.position.y * dimensions + p.position.x 
    texture.image.data[i*4]   = p.color.x
    texture.image.data[i*4+1] = p.color.y
    texture.image.data[i*4+2] = p.color.z
    texture.image.data[i*4+3] = 255 })
  return texture }

// setup the seed composition
// Used to transform input data (seed points) into a format
// that's ready to be processed by JFA (a buffer of pixels where each red-green
// and blue-alpha pair specifies a grid location).
// ————————————————————————————
function _prepreJFA(renderer, dimensions, points𝔗) {
  let shader      = { uniforms:       UniformsUtils.clone( StipplingShaders[ 'prepareJFA' ].uniforms ),
                      vertexShader:   StipplingShaders[ 'prepareJFA' ].vertexShader,
                      fragmentShader: StipplingShaders[ 'prepareJFA' ].fragmentShader},
      material    = new ShaderMaterial(shader),
      params      = { minFilter: LinearFilter, magFilter: LinearFilter, format: RGBFormat, stencilBuffer: false },
      target      = new WebGLRenderTarget( dimensions, dimensions, params),
      composition = new EffectComposer( renderer, target ),
      shaderPass  = new ShaderPass(material),
      saveTarget  = new WebGLRenderTarget( dimensions, dimensions, params),
      savePass    = new SavePass(saveTarget)
  
  composition.addPass( shaderPass )
  composition.addPass( savePass )
  return { composition, shader: material, target: saveTarget}}

function _jfa(renderer, dimensions) {
  let shader      = { uniforms:       UniformsUtils.clone( StipplingShaders[ 'JFA' ].uniforms ),
                      vertexShader:   StipplingShaders[ 'JFA' ].vertexShader,
                      fragmentShader: StipplingShaders[ 'JFA' ].fragmentShader},
      material    = new ShaderMaterial(shader),
      params      = { minFilter: LinearFilter, magFilter: LinearFilter, format: RGBFormat, stencilBuffer: false },
      target      = new WebGLRenderTarget( dimensions, dimensions, params),
      composition = new EffectComposer( renderer, target ),
      shaderPass  = new ShaderPass(material),
      saveTarget  = new WebGLRenderTarget( dimensions, dimensions, params),
      savePass    = new SavePass(saveTarget)
  
  composition.addPass( shaderPass )
  composition.addPass( savePass )
  return { composition, shader: material, target: saveTarget}}

function _runJumpFloodStep(jfa, stepSize) {
  jfa.shader.uniforms.iStepSize.value   = stepSize
  jfa.composition.render(0)
  jfa.shader.uniforms.tInput.value      = jfa.target.texture }

function _voronoi(renderer, dimensions) {
  let shader      = { uniforms:       UniformsUtils.clone( StipplingShaders[ 'voronoi' ].uniforms ),
                      vertexShader:   StipplingShaders[ 'voronoi' ].vertexShader,
                      fragmentShader: StipplingShaders[ 'voronoi' ].fragmentShader},
      material    = new ShaderMaterial(shader),
      params      = { minFilter: LinearFilter, magFilter: LinearFilter, format: RGBFormat, stencilBuffer: false },
      target      = new WebGLRenderTarget( dimensions, dimensions, params),
      composition = new EffectComposer( renderer, target ),
      shaderPass  = new ShaderPass(material),
      saveTarget  = new WebGLRenderTarget( dimensions, dimensions, params),
      savePass    = new SavePass(saveTarget)

  composition.addPass( shaderPass )
  composition.addPass( savePass )
  return { composition, shader: material, target: saveTarget}}


function init(renderer, dimensions, points) {
    console.log('initialize JFA')
    // console.log('points', JSON.stringify(points, null, 2))

    let points𝔗     = _makePointsTexture(dimensions, points),
        prepareJFA  = _prepreJFA(renderer, dimensions, points𝔗),
        jfa         = _jfa(renderer, dimensions),
        voronoi     = _voronoi(renderer, dimensions)

    renderer.clear()
    prepareJFA.shader.uniforms.tSeeds.value           =  points𝔗
    prepareJFA.shader.uniforms.iBackgroundColor.value = new Vector4(0, 0, 0, 0)
    prepareJFA.shader.uniforms.iResolution.value      = dimensions
   
    jfa.shader.uniforms.tInput.value      = prepareJFA.target.texture
    jfa.shader.uniforms.iResolution.value = dimensions
   
    
    voronoi.shader.uniforms.tSeeds.value          = points𝔗
    // voronoi.shader.uniforms.tInput.value          = prepareJFA.target.texture
    voronoi.shader.uniforms.tInput.value          = jfa.target.texture
    voronoi.shader.uniforms.iResolution.value     = dimensions
    voronoi.composition.render(0)

    // // screen rendering
    let targetParams = {
        minFilter: LinearFilter,
        magFilter: LinearFilter,
        format: RGBFormat,
        stencilBuffer: false,
      },
      renderTarget = new WebGLRenderTarget(
        window.innerWidth,
        window.innerHeight,
        targetParams
      ),
      composition = new EffectComposer(renderer, renderTarget),
      // texturePass     = new TexturePass(points𝔗)
      // texturePass     = new TexturePass(prepareJFA.target.texture)
      // texturePass     = new TexturePass(jfa.target.texture)
      texturePass = new TextureEffect(voronoi.target.texture);

    composition.addPass( texturePass )
    texturePass.renderToScreen = true
    // composition.render(0)

    // run…
    // ————————————————————————————
    let clock = new Clock(), 
        last  = performance.now(),
        ι     = 0,
        δ, now, stepSize
    
    function render() { 
      // requestAnimationFrame(render)  // reschedule
      now = performance.now()         // timing
      δ = (now - last) / 1000
      if (δ > 1) δ = 1 // safety cap on large deltas
      last = now

      // renderer.clear()
      prepareJFA.composition.render(δ)

      // jfa.shader.uniforms.tInput.value      = prepareJFA.target.texture
    
      stepSize = dimensions/2
      while(stepSize >= 1) {
        _runJumpFloodStep(jfa, stepSize)
        stepSize = stepSize/2 }
      _runJumpFloodStep(jfa, 2)
      _runJumpFloodStep(jfa, 1)

      voronoi.composition.render(0)
      
      composition.render(δ)

    }

    console.log('here we go —→')
    render() 
      

}

export default { init }