import {
    AmbientLight,
    BackSide,
    Box3,
    BoxGeometry,
    BoxBufferGeometry,
    Clock,
    Color,
    DataTexture,
    FlatShading,
    Fog,
    ImageUtils,
    LinearFilter,
    LoadingManager,
    Math as ThreeMath,
    Mesh,
    NearestFilter,
    OrthographicCamera,
    PerspectiveCamera,
    PlaneBufferGeometry,
    PlaneGeometry,
    RepeatWrapping,
    RGBFormat,
    RGBAFormat,
    Scene,
    ShaderMaterial,
    SmoothShading,
    Texture,
    TextureLoader,
    UniformsUtils,
    Vector2,
    Vector3,
    Vector4,
    WebGLRenderer,
    WebGLRenderTarget
} from 'three'

import {
  BloomEffect,
  ClearPass,
  DepthPass,
  DotScreenPass,
  EffectComposer,
  FilmPass,
  GlitchPass,
  GodRaysPass,
  PixelationPass,
  RenderPass,
  SavePass,
  ShaderPass,
  SMAAPass,
  TextureEffect,
} from "postprocessing";

import * as pp from 'postprocessing'

console.log("postprocessing", pp);

import _ from 'lodash'
import detector from '../../js/gl-detector'
import StipplingShaders from './stippling-shaders'
import JFA from './jfa'


const DEFAULT_OPTIONS = {
    autoplay: false,
    loop: false,
    duration: 8000
}


function _loadTexture(τ) {
    return new Promise((resolve, reject) => {
        new TextureLoader().load(τ, texture => resolve(texture))
    })
}

let indexColors = [
    new Vector3(0, 255, 255),
    new Vector3(255, 0, 255),
    new Vector3(255, 255, 0)]

function _indexColor(i, n) {
    let brightness = (i + 1) * 254 / n
    // return new Vector3(brightness, brightness, brightness)
    return new Vector3(_.random(254), _.random(254), _.random(254))
    // return new Vector3(_.random(128, 254), _.random(128, 254), _.random(128, 254))
    // return new Vector3(_.random(127), _.random(127), _.random(127))
}

// create a plain data texture with the given dimensions δ
function _makeDataTexture(δ) {
    let dataMap = new Uint8Array(1 << (Math.floor(Math.log(δ.x * δ.y * 4) / Math.log(2)))),

        // DataTexture( data, width, height, format, type, mapping, wrapS, wrapT, magFilter, minFilter, anisotropy )
        texture = new DataTexture(dataMap, δ.x, δ.y, RGBAFormat)

    // initialize the data map all black and totally transparent
    _(dataMap)
        .chunk(4)                 // split into pixel chunks
        .each((δ, ι) => {
            dataMap[ι * 4] = 0     // r
            dataMap[ι * 4 + 1] = 0     // g
            dataMap[ι * 4 + 2] = 0     // b
            dataMap[ι * 4 + 3] = 0
        })  // α

    texture.needsUpdate = true
    return texture
}


function _update(δ) {
}

function _run(renderer, composition) {

}

// Compose
// ————————————————————————————————
function _compose({ renderer, texture, voronoi }) {
    let renderTarget = new WebGLRenderTarget(window.innerWidth, window.innerHeight),
        composition = new EffectComposer(renderer, renderTarget),
        texturePass = new TextureEffect(texture)
        // bloomPass = new BloomPass({ resolutionScale: 0.5, intensity: 0.2 }),
        // voronoiPass = new ShaderPass(voronoi)

    // bleachShader    = { uniforms:       UniformsUtils.clone( ShaderExtras[ 'bleachbypass' ].uniforms ),
    //                     vertexShader:   ShaderExtras[ 'bleachbypass' ].vertexShader,
    //                     fragmentShader: ShaderExtras[ 'bleachbypass' ].fragmentShader},
    // bleachMaterial  = new ShaderMaterial(bleachShader),
    // bleachPass      = new ShaderPass( bleachMaterial ),

    composition.addPass(texturePass)
    // composition.addPass( voronoiPass )
    // composition.addPass( bloomPass )

    texturePass.renderToScreen = true
    // voronoiPass.renderToScreen = true
    // bloomPass.renderToScreen = true

    console.log('composition ready')
    return composition
}

function _initGL(container, image, options) {
    options = _.defaults(options, DEFAULT_OPTIONS)

    // Renderer
    // ————————————————————————————
    let renderer = new WebGLRenderer()
    container.appendChild(renderer.domElement)
    renderer.setPixelRatio(window.devicePixelRatio)
    renderer.setSize(window.innerWidth, window.innerHeight)
    renderer.setClearColor(0x000000)

    console.log('renderer ready')


    // Texture
    // ————————————————————————————  
    let τ = image && (_.isString(image) ? image : image.nodeName && image.src) || ''
    
    console.log('texture', τ)
    _loadTexture(τ)
        .then(image𝔗 => {
            image𝔗.minFilter = LinearFilter
            let size = 64,
                n = 4,
                points = _(n).range()
                    .map(i => {
                        let
                            // x = i * size/n + (size/n)/2,
                            // y = i * size/n + (size/n)/2 + (2 * i),
                            x = _.random(size - 1),
                            y = _.random(size / 2),
                            // y = size/2,
                            position = new Vector2(x, y),
                            color = _indexColor(i, n)
                        return { color, position }
                    })
                    .value()

            JFA.init(renderer, size, points)

            // run…
            // ————————————————————————————
            let clock = new Clock(),
                last = performance.now(),
                ι = 0,
                δ, now

            function render() {
                // requestAnimationFrame(render)  // reschedule
                now = performance.now()         // timing
                δ = (now - last) / 1000
                if (δ > 1) δ = 1 // safety cap on large deltas
                last = now

                jfa𝐌.uniforms.iLevel.value = 1

                renderer.clear()
                composition.render(δ)

                // jfa𝐌.uniforms.tSeeds.value  = jfaℭ.writeBuffer.texture.clone()
                // jfa𝐌.uniforms.iLevel.value = ι

                // ι += 1
                // if(ι >= 9) ι = 0
            }

            // render() 

        })
}

function init(image𝔗, options) {
    // check that the browser is capable of handling WebGL
    if (!detector.webgl) {
        detector.addGetWebGLMessage()
        console.log('no webgl.')
        return
    }
    else _initGL(image𝔗, options)
}

export default { init }