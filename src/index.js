'use strict'

// requires for webpack
// listing these files here is necessary, since webpack won't find them otherwise
require("./styles.scss")
require("../assets/fonts/IBMPlexMono-Regular.woff2")
require("../assets/cv.txt")

import {init as initContent} from './js/projects'
// import {init as initSVG} from './js/svg-foreground'


function initialize() {
    // initGl(document.getElementById('gl'), window.innerWidth, window.innerHeight)
    // initSVG(document.getElementById('main'), window.innerWidth, window.innerHeight)
    initContent(document.getElementById("projects"));
}

function ready(fn) {
    if (document.readyState != 'loading'){
       fn()
    } else {
      document.addEventListener('DOMContentLoaded', fn)
    }
  }

  ready(initialize)
